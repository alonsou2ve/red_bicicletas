var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicleta', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'conection error'));
        db.on('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });


    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        if ('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function (err, newbici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Devolver la bici con codigo 1', (done) => {
            var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(aBici, function (err, newbici) {
                if (err) console.log(err);
                    var aBici2 = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
                    Bicicleta.add(aBici2, function (err, newbici) { 
                    if (err) console.log(err);
                    Bicicleta.findByCode(1, function (error, targetBici) {
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);
                        done();
                    });
                });
            });
        });
    });



   

});

/*
beforeEach(()=>{
    Bicicleta.allBicis = [];
})



describe('Bicicleta.allBicis',() => {
    it('Comienza vacia',()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add',()=>{
    it('Agregamos una;',()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana', [-34.60122424,-58.38611497]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});

describe('Bicicleta.findById',()=>{
    it('Debe devolver la bicicleta con ID 1', ()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana', [-34.60122424,-58.38611497]);
        var b = new Bicicleta(2,'azul','montana', [-34.59122424,-58.37611497]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe('rojo');
        expect(targetBici.modelo).toBe('urbana');
    });
});

describe('Bicicleta.RemoveById',()=> {
    it('Debe eliminar el registro con el ID 1', ()=> {expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana', [-34.60122424,-58.38611497]);
        var b = new Bicicleta(2,'azul','montana', [-34.59122424,-58.37611497]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var longitud_original = Bicicleta.allBicis.length;
        var targetBici = Bicicleta.removeById(1);
        var longitud_eliminado = Bicicleta.allBicis.length;

        expect(longitud_eliminado).toBe(longitud_original-1);
    });

});
*/