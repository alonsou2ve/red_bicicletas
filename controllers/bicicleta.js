const Bicicleta = require('../models/bicicleta');


exports.bicicleta_list = (req,res)=>{
    Bicicleta.find({}, (err, bicicletas) => {
        res.render('bicicletas/index', { bicis: bicicletas });
    });
}

exports.bicicleta_create_get = (req,res)=>{
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = (req, res)=> {
    var bici = new Bicicleta({id: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});
    Bicicleta.create(bici);
     res.redirect('/bicicletas');
 }


 exports.bicicleta_update_get = (req,res)=>{
    var bici=Bicicleta.findById(req.params.id, function (err, bici) {
        res.render('bicicletas/update', {bici: bici});
    });
} 




 exports.bicicleta_update_post = (req,res, next) => {
     bici2  = {
         color: req.body.color,
         modelo: req.body.modelo,
         ubicacion: [req.body.lat,req.body.lng]
     }
    var bici= Bicicleta.findByIdAndUpdate(req.params.id, bici2, {new: true}, function(err, bici) 
    {
        res.redirect('/bicicletas');
    })

}


 
exports.bicicleta_delete_post = (req,res, next) => {
    Bicicleta.findByIdAndRemove( req.body.id )
    .then( () => {
        res.redirect('/bicicletas');
        next();
    } )
    .catch( error => {
     console.log( `Error deleting subscriber by ID: ${error.message}` );
     next();
    } );
}




/* Sin MongoDB

var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis})
}

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res) {
   var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
   bici.ubicacion = [req.body.lat, req.body.lng];
   Bicicleta.add(bici);

   res.redirect('/bicicletas');
}


exports.bicicleta_update_get = function(req, res) {
    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = function(req, res) {
   var bici = Bicicleta.findById(req.params.id);
   bici.id = req.body.id;
   bici.color = req.body.color;
   bici.modelo = req.body.modelo;
   bici.ubicacion = [req.body.lat, req.body.lng];
   res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req,res) {
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
} 

*/