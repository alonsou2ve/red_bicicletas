const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

var reservarSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta' },
  usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' }
});

reservarSchema.methods.diasReserva = () => {
  return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
};

module.exports = mongoose.model('reserva', reservarSchema);