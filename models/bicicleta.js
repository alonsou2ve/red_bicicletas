const mongoose = require('mongoose');
const schema = mongoose.Schema;

const bicicletaSchema = new schema({
    code: {
        type: Number
    },
    color: {
        type: String
    },  
    modelo:{ type: String},
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function(code,color,modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.pre('save', function (next) {
    if (this.isNew && Array.isArray(this.ubicacion) && 0 === this.ubicacion.length) {
      this.ubicacion = undefined;
    }
    next();
  })

bicicletaSchema.methods.toString = function(){
    return `code: ${this.code} | color: ${this.color}`;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
};

bicicletaSchema.statics.add = function(bici, cb) {
    return this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function(code,cb){
    return this.findOne({code: code},cb);
}

bicicletaSchema.statics.removeByCode = function(code,cb){
    return this.deleteOne({code: code},cb);
}

module.exports = mongoose.model('Bicicleta',bicicletaSchema);


/*  No incluye mongo BD

var Bicicleta = function (id, color, modelo, ubicacion)  {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}    

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x=>x.id == aBiciId);
    if (aBici)
        return aBici;
    else
         throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

 var a = new Bicicleta(1,'rojo','urbana', [-34.60122424, -58.3861497]);
var b = new Bicicleta(2,'blanca','urbana', [-34.596932, -58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);


module.exports = Bicicleta;

*/